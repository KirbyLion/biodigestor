#include <OneWire.h>
#include <DallasTemperature.h>

// Pino selecionado para enviar os dados para o Arduino
#define ONE_WIRE_BUS 2

// Configura uma instância oneWire para se comunicar com qualquer OneWire dispositivos (não apenas ICs Maxim/Dallas )
OneWire oneWire(ONE_WIRE_BUS);

// Passa a referência oneWire para Dallas Temperature. 
DallasTemperature sensors(&oneWire);

void setup(void)
{
  // Começa a porta serial
  Serial.begin(9600);
  Serial.println("Dallas Temperature IC Control Library Demo");

  // Inicia a biblioteca
  sensors.begin();
}

void loop(void)
{ 
  // Chama sensors.requestTemperatures() para emitir uma temperatura global 
  // Solicita todos os dispositivos no bus
  Serial.print("Requesting temperatures...");
  sensors.requestTemperatures(); // Manda o comando para pegar as temperaturas
  Serial.println("DONE");
  
  Serial.print("Temperature for the device 1 (index 0) is: ");
  Serial.println(sensors.getTempCByIndex(0));
   delay(1000);  
}
