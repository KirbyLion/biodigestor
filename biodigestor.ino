

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <OneWire.h>
#include <DallasTemperature.h>

// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 2

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

String myString = ""; // complete message from arduino, which consistors of snesors data
char rdata; // received charactors

HTTPClient http;
 
int firstVal, secondVal,thirdVal; // sensors 
const char *ssid = "biodig";  //ENTER YOUR WIFI SETTINGS
const char *password = "senha100senha"; 

 
//===============================================================
//                  SETUP
//===============================================================
void setup(void){
  Serial.begin(9600);
  Serial.println("");
  WiFi.mode(WIFI_OFF);        //Prevents reconnection issue (taking too long to connect)
  delay(1000);
  WiFi.mode(WIFI_STA);
  
  //Serial.println("HTTP server started");
  WiFi.begin(ssid, password);
  Serial.println("");

  Serial.print("Connecting");
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP
    sensors.begin();
}
//===============================================================
//                     LOOP
//===============================================================
void loop(void){
  
  Serial.println(">>> MONITORAMENTO BIODIGESTOR <<<");
  
// ==== LEITURA DOS SENSORES ====
  sensors.requestTemperatures(); // Send the command to get temperatures
  Serial.println("Lendo dados dos sensores...");
  
  float t = sensors.getTempCByIndex(0);
  //Serial.print("Temperatura: ");
  //Serial.println(t); 
  
  //if (Serial.available() > 0 ) 
  //{
    rdata = Serial.read(); 
    myString = myString+ rdata; 
    Serial.print(rdata);
    /*if( rdata != '\n')
    {
      Serial.println("Xii! Problema a leitura pela serial...");
      Serial.println("Usando valores padrão...");
      //myString = "80,1";
    }
  }
   else {
    Serial.println("Opa! Falhou a leitura pela serial. Olha os fios do arduino!");
    Serial.println("Usando valores padrão...");
    //myString = "80,1";
    }*/
   
    Serial.println(myString); 

    int sensor_pin = A0;
    int output_value = 0;
    int nivel_sensor = 300;
    
    String l = getValue(myString, ',', 0);
    String m = getValue(myString, ',', 1);
  
    int umidade = l.toInt();
    int gas = m.toInt();
    if ( gas > nivel_sensor)
  {
    // Alerta que há presença
    // de gás
    gas = 1;
  }
  else
  {
    // Alerta que não há gás
    gas = 0;
  }
    output_value = analogRead(sensor_pin);
    umidade = map(output_value,1023,0,0,100);
    //int temperatura = t.toInt();
  
  // ===== ENVIO DOS DADOS =====
  
    DynamicJsonDocument JSONencoder(1024);
  
    JSONencoder["umidade"] = umidade;
    JSONencoder["gas"] = gas;
    JSONencoder["temperatura"] = t;
  
    char JSONmessageBuffer[300];
    serializeJsonPretty(JSONencoder, JSONmessageBuffer);
    //serializeJsonPretty(PostData, JSONmessageBuffer); 
        
    Serial.println("Dados enviados: ");
    Serial.println(JSONmessageBuffer);
    
    http.begin("http://10.42.0.1/persons");              //Specify request destination
    http.addHeader("Content-Type", "application/json");  //Specify content-type header
  
    //int httpCode =
    http.POST(JSONmessageBuffer);   //Send the request
    //String payload =
    http.getString();    //Get the response payload
  
    //Serial.print("httpCode: ");
    //Serial.println(httpCode);   //Print HTTP return code
    //Serial.print("payload: ");
    //Serial.println(payload);    //Print request response payload
    Serial.println();
   
    http.end();  //Close connection
  
    myString = "";
  
    delay(5000);  //Post Data at every 5 seconds
}

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;
 
    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
